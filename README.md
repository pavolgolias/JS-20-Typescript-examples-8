## JS-20 (Typescript-8) - Typescript example project
This is an example Typescript project covering multiple Typescript features. Based on UDEMY course: https://www.udemy.com/understanding-typescript.

These examples are focusing on **using JS libraries together with TypeScript**.

For more information about package translation see https://github.com/DefinitelyTyped/DefinitelyTyped .

#### Used commands and libs:
* npm init
* npm install lite-server --save-dev
* tsc <filename.ts>
* tsc --init
* tsc
* tsc --outFile app.js circleMath.ts rectangleMath.ts app.ts 
* tsc app.ts --outFile app.js // if we use reference 
* npm install systemjs --save
* npm install jquery --save 


* npm install -g typings
* typings install dt~jquery --global --save


* npm install --save-dev @types/jquery
